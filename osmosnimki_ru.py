title  = 'Osmosnimki'
format = 'png'
key    = 'k'
proj   = 3857

def url(x, y, z):
	return 'http://a.tile.osmosnimki.ru/kosmo/{z}/{x}/{y}'.format(x=x, y=y, z=z)
