title  = 'Yandex Satellite'
format = 'jpg'
key    = 'y'
proj   = 3395

def url(x, y, z):
	return 'http://sat00.maps.yandex.net/tiles?l=sat&x={x}&y={y}&z={z}'.format(x=x, y=y, z=z)
