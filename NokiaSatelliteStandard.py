title  = 'Nokia Satellite Standard'
format = 'jpg'
key    = 'n'
proj   = 3857

def url(x, y, z):
	res = ''
	os_x = os_y = pr_x = pr_y = 2 ** z / 2
	for i in range(2, z + 2):
		pr_x /= 2;
		pr_y /= 2;
		if x < os_x:
			os_x -= pr_x
			if y < os_y:
				os_y -= pr_y
				res += '0'
			else:
				os_y += pr_y
				res += '2'
		else:
			os_x += pr_x
			if y < os_y:
				os_y -= pr_y
				res += '1'
			else:
				os_y += pr_y
				res += '3'
	return 'http://stg.lbsp.navteq.com/satellite/6.0/images/' \
		+ '?token=LBSP_DEV_ALL&profile=ColorOnly' \
		+ '&quadkey=' + res
