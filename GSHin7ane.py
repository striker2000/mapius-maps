title  = 'Genshtab in7ane'
format = 'jpg'
key    = '7'
proj   = 3857

def url(x, y, z):
	return 'http://www.in7ane.com/topomaps/tiles/{z}/{x}/{y}.jpg'.format(x=x, y=y, z=z)
