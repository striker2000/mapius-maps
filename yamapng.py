title  = 'Yandex Map'
format = 'png'
key    = 'q'
proj   = 3395

def url(x, y, z):
	return 'http://vec01.maps.yandex.net/tiles?l=map&x={x}&y={y}&z={z}'.format(x=x, y=y, z=z)
