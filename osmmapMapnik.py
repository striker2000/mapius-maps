title  = 'OSM Mapnik'
format = 'png'
key    = 'o'
proj   = 3857

def url(x, y, z):
	return 'http://a.tile.openstreetmap.org/{z}/{x}/{y}.png'.format(x=x, y=y, z=z)
